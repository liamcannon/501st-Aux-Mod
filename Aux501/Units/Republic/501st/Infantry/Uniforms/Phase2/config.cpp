class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_Uniform_Base",
            "Aux501_Units_Republic_501st_Standard_Uniform",
            "Aux501_Units_Republic_501st_Cadet_Uniform",
            "Aux501_Units_Republic_501st_Trooper_Uniform",
            "Aux501_Units_Republic_501st_Snr_Trooper_Uniform",
            "Aux501_Units_Republic_501st_Vet_Trooper_Uniform",
            "Aux501_Units_Republic_501st_Lance_CP_Uniform",
            "Aux501_Units_Republic_501st_CP_Uniform",
            "Aux501_Units_Republic_501st_Snr_CP_Uniform",
            "Aux501_Units_Republic_501st_CS_Uniform",
            "Aux501_Units_Republic_501st_Snr_CS_Uniform",
            "Aux501_Units_Republic_501st_CSM_Uniform",
            "Aux501_Units_Republic_501st_2LT_Uniform",
            "Aux501_Units_Republic_501st_1LT_Uniform",
            "Aux501_Units_Republic_501st_Captain_Uniform",
            "Aux501_Units_Republic_501st_Major_Uniform"
        };
    };
};

class CfgWeapons
{
    class Uniform_Base;
    class UniformItem;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        scope = 1;
        scopeArsenal = 1;
        author = "501st Aux Team";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_uniform_ca.paa";
        model = "\SWLB_groundholders\SWLB_clone_uniform_gh.p3d";
        class ItemInfo: UniformItem
        {
            uniformClass = "Aux501_Units_Republic_501st_Unit_Base";
            armor = 100;
            armorStructural = 5;
            explosionShielding = 1.1;
            impactDamageMultiplier = -100;
            modelSides[] = {6};
            uniformType = "Neopren";
            containerClass = "Supply100";
            mass = 40;
        };
    };

    class Aux501_Units_Republic_501st_Standard_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 1;
        scopeArsenal = 1;
        displayName = "[501st] INF P2 ARMR 00";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Recruit_Unit";
        };
    };
    class Aux501_Units_Republic_501st_Cadet_Uniform: Aux501_Units_Republic_501st_Standard_Uniform
    {
        displayName = "[501st] INF P2 ARMR 01 - CR-C";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Cadet_Unit";
        };
    };
    class Aux501_Units_Republic_501st_Trooper_Uniform: Aux501_Units_Republic_501st_Standard_Uniform
    {
        picture = "\SWLB_units\data\ui\icon_SWLB_clone_501stTrooper_uniform_ca.paa";
        displayName = "[501st] INF P2 ARMR 02 - CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Trooper_Unit";
        };
    };
    class Aux501_Units_Republic_501st_Snr_Trooper_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 03 - Sr. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_Unit";
        };
    };
    class Aux501_Units_Republic_501st_Vet_Trooper_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 04 - Vet. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_Unit";
        };
    };
    class Aux501_Units_Republic_501st_Lance_CP_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 05 - CLC";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Lance_CP_Unit";
        };
    };
    
    //NCOs
    class Aux501_Units_Republic_501st_CP_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 06 - CP";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_CP_Unit";
        };
    };
    class Aux501_Units_Republic_501st_Snr_CP_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 07 - Sr. CP";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_CP_Unit";
        };
    };
    class Aux501_Units_Republic_501st_CS_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 08 - CS";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_CS_Unit";
        };
    };
    class Aux501_Units_Republic_501st_Snr_CS_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 09 - Sr. CS";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_CS_Unit";
        };
    };
    class Aux501_Units_Republic_501st_Platoon_CSM_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 10 - CS-M";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Platoon_CSM_Unit";
        };
    };
    
    //Officers
    class Aux501_Units_Republic_501st_2LT_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 11 - 2nd Lt";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_2LT_Unit";
        };
    };
    class Aux501_Units_Republic_501st_1LT_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 12 - 1st Lt";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_1LT_Unit";
        };
    };
    class Aux501_Units_Republic_501st_Captain_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 13 - Captain";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Captain_Unit";
        };
    };
    class Aux501_Units_Republic_501st_Major_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 14 - Major";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Major_Unit";
        };
    };
};