class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Backpacks_Beltbag
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Backpacks_Beltbag"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_base;

    class Aux501_Units_Republic_501_Infantry_Backpacks_Beltbag: Aux501_Units_Republic_501_Infantry_Backpacks_base
    {
        scope = 1;
        scopeArsenal = 1;
        displayName = "[501st] INF Beltbag 01";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_belt_bag_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBeltBag.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_belt_bag_co.paa"};
    };
};