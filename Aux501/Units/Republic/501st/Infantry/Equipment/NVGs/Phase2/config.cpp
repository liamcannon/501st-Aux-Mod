class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_base;
   
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV: Aux501_Republic_501_Infantry_Equipment_NVGs_base
    {
        scope = 1;
        scopearesenal = 1;
        picture = "\MRC\JLTS\Core_mod\data\ui\nvg_chip_1_ui_ca.paa";
        displayName = "[501st] INF NVG 00 (NV)";
        model = "\MRC\JLTS\weapons\E5S\E5S_mag.p3d";
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV
    {
        displayName = "[501st] INF NVG 01 (TI)";
        visionMode[] = {"Normal","NVG","TI"};
    };
};