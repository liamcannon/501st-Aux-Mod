class cfgPatches
{
    class Aux501_Patch_Units_Republic_212_Vests_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_212th_Officer_Vest"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_2LT_3;

    class Aux501_Units_Republic_212th_Officer_Vest: Aux501_Units_Republic_501_Infantry_Vests_2LT_3
    {
        scope = 2;
        scopeArsenal = 2;
        displayname = "[212th] Pauldron 01 - Officer";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_212thOfficer_vest_co.paa"};
    };
};