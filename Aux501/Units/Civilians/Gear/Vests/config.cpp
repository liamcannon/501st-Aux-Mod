class cfgPatches
{
    class Aux501_Patch_Units_Civilians_Vests
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class cfgWeapons
{
    class V_Safety_orange_F;

    class Aux501_Units_Civilians_Vests_Republic_Engineer: V_Safety_orange_F
    {
        author = "501st Aux Team";
        displayname = "[501st] REP Engineer Vest";
        hiddenSelectionsTextures[] = {"Aux501\Units\Civilians\Gear\Vests\data\textures\Republic_engineer_vest.paa"};
    };
};