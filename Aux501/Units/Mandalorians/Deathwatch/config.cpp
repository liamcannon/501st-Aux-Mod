class cfgPatches
{
    class Aux501_Patch_Units_Mandalorians_Deathwatch
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_Mandalorian_Deathwatch_Soldier_Unit",
            "Aux501_Units_Mandalorian_Deathwatch_NCO_Unit",
            "Aux501_Units_Mandalorian_Deathwatch_Captain_Unit",
            "Aux501_Units_Mandalorian_Deathwatch_Marksmen_Unit",
            "Aux501_Units_Mandalorian_Deathwatch_AT_Unit",
            "Aux501_Units_Mandalorian_Deathwatch_AA_Unit",
            "Aux501_Units_Mandalorian_Deathwatch_Shotgunner_Unit",
            "Aux501_Units_Mandalorian_Deathwatch_Support_Unit"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class I_Soldier_02_F;

    class Aux501_Units_Mandalorian_Deathwatch_Soldier_Unit: I_Soldier_02_F
    {
        displayName = "Soldier";
        scope = 2;
        scopeCurator = 2;
        side = 0;
        faction = "Aux501_FactionClasses_Mandos";
        editorCategory = "Aux501_Editor_Category_Mandalorians";
        editorSubcategory = "Aux501_Editor_Subcategory_Deathwatch";
        editorPreview = "\ls_units_greenfor\mandalorian\ui\ls_mando_base_assault.jpg";
        icon = "LSiconRifleman";
        role = "Rifleman";
        identityTypes[] = {"LanguageENGB_F","Head_NATO"};
        uniformClass = "Aux501_Units_Mandalorian_Undersuit_Uniform";
        backpack = "Aux501_Units_Mandalorian_Backpacks_Jumppack";
        model = "\A3\Characters_F_Bootcamp\Guerrilla\ig_guerrilla_6_1.p3d";
        hiddenSelections[] = {"camo","insignia"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Mandalorians\Deathwatch\Uniforms\data\textures\mando_undersuit.paa"};
        linkedItems[] = 
        {
            "Aux501_Units_Mandalorian_Deathwatch_Helmet_Soldier",

            "Aux501_Units_Units_Mandalorian_Vests_Deathwatch_Trooper",

            "ItemMap",
            "ItemCompass",
            "ACE_Altimeter",
            "SWLB_comlink_droid",
            "ls_mandalorian_rangefinder_nvg"
        };
        respawnLinkedItems[] = 
        {
            "Aux501_Units_Mandalorian_Deathwatch_Helmet_Soldier",

            "Aux501_Units_Units_Mandalorian_Vests_Deathwatch_Trooper",

            "ItemMap",
            "ItemCompass",
            "ACE_Altimeter",
            "SWLB_comlink_droid",
            "ls_mandalorian_rangefinder_nvg"
        };
        weapons[] = 
        {
            "Aux501_Weaps_Westar35C",
            "Aux501_Weaps_Westar35SA",
            "Throw",
            "Put"
        };
        respawnWeapons[] = 
        {
            "Aux501_Weaps_Westar35C",
            "Aux501_Weaps_Westar35SA",
            "Throw",
            "Put"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",

            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_Smoke_White"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",

            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",

            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_Smoke_White"
        };
        items[] = 
        {
            "ACE_packingBandage",
            "ACE_packingBandage",
            "ACE_packingBandage"
        };
        respawnItems[] = 
        {
            "ACE_packingBandage",
            "ACE_packingBandage",
            "ACE_packingBandage"
        };

        impactEffectsBlood = "ImpactMetal";
        impactEffectsNoBlood = "ImpactPlastic";
        maxSpeed = 24;
        maxTurnAngularVelocity = 3;
        lyingLimitSpeedHiding = 0.8;
        crouchProbabilityHiding = 0.8;
        lyingLimitSpeedCombat = 1.8;
        crouchProbabilityCombat = 0.4;
        crouchProbabilityEngage = 0.75;
        lyingLimitSpeedStealth = 2;
        cost = 100000;
        canHideBodies = 1;
        detectSkill = 12;
        audible = 0.04;
        camouflage = 0.4;
        accuracy = 0.7;
        sensitivityEar = 0.125;
        sensitivity = 1.75;
        threat[] = {1,0.5,0.1};
        canCarryBackPack = 1;
    };
    class Aux501_Units_Mandalorian_Deathwatch_NCO_Unit: Aux501_Units_Mandalorian_Deathwatch_Soldier_Unit
    {
        displayName = "NCO";
        icon = "iconManLeader";
        role = "Marksman";
        cost = 350000;
        accuracy = 3.9;
        linkedItems[] = 
        {
            "Aux501_Units_Mandalorian_Deathwatch_Helmet_NCO",

            "Aux501_Units_Units_Mandalorian_Vests_Deathwatch_NCO",

            "ItemMap",
            "ItemCompass",
            "ACE_Altimeter",
            "SWLB_comlink_droid",
            "ls_mandalorian_rangefinder_nvg"
        };
        respawnLinkedItems[] = 
        {
            "Aux501_Units_Mandalorian_Deathwatch_Helmet_NCO",
            
            "Aux501_Units_Units_Mandalorian_Vests_Deathwatch_NCO",

            "ItemMap",
            "ItemCompass",
            "ACE_Altimeter",
            "SWLB_comlink_droid",
            "ls_mandalorian_rangefinder_nvg"
        };
    };
    class Aux501_Units_Mandalorian_Deathwatch_Captain_Unit: Aux501_Units_Mandalorian_Deathwatch_NCO_Unit
    {
        displayName = "Captain";
        icon = "iconManOfficer";
        cost = 350000;
        weapons[] = 
        {
            "Aux501_Weaps_Westar35SA",
            "Throw",
            "Put"
        };
        respawnWeapons[] = 
        {
            "Aux501_Weaps_Westar35SA",
            "Throw",
            "Put"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_Smoke_White"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_Smoke_White"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Mandalorian_Deathwatch_Helmet_Captain",

            "Aux501_Units_Units_Mandalorian_Vests_Deathwatch_Captain",
            
            "ItemMap",
            "ItemCompass",
            "ACE_Altimeter",
            "SWLB_comlink_droid",
            "ls_mandalorian_rangefinder_nvg"
        };
        respawnLinkedItems[] = 
        {
            "Aux501_Units_Mandalorian_Deathwatch_Helmet_Captain",

            "Aux501_Units_Units_Mandalorian_Vests_Deathwatch_Captain",
            
            "ItemMap",
            "ItemCompass",
            "ACE_Altimeter",
            "SWLB_comlink_droid",
            "ls_mandalorian_rangefinder_nvg"
        };
    };
    class Aux501_Units_Mandalorian_Deathwatch_Marksmen_Unit: Aux501_Units_Mandalorian_Deathwatch_Soldier_Unit
    {
        displayName = "Marksmen";
        icon = "LSiconMarksman";
        linkedItems[] = 
        {
            "Aux501_Units_Mandalorian_Deathwatch_Helmet_NiteOwl",

            "Aux501_Units_Units_Mandalorian_Vests_Deathwatch_Trooper",

            "ItemMap",
            "ItemCompass",
            "ACE_Altimeter",
            "SWLB_comlink_droid",
            "ls_mandalorian_rangefinder_nvg"
        };
        respawnLinkedItems[] = 
        {
            "Aux501_Units_Mandalorian_Deathwatch_Helmet_NiteOwl",

            "Aux501_Units_Units_Mandalorian_Vests_Deathwatch_Trooper",

            "ItemMap",
            "ItemCompass",
            "ACE_Altimeter",
            "SWLB_comlink_droid",
            "ls_mandalorian_rangefinder_nvg"
        };
        weapons[] = 
        {
            "Aux501_Weaps_Beviin24_scoped",
            "Throw",
            "Put"
        };
        respawnWeapons[] = 
        {
            "Aux501_Weaps_Beviin24_scoped",
            "Throw",
            "Put"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",

            "Aux501_Weapons_Mags_CISDetonator",

            "Aux501_Weapons_Mags_Smoke_White"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",
            "Aux501_Weapons_Mags_Beviin2415",

            "Aux501_Weapons_Mags_CISDetonator",

            "Aux501_Weapons_Mags_Smoke_White"
        };
    };
    class Aux501_Units_Mandalorian_Deathwatch_AT_Unit: Aux501_Units_Mandalorian_Deathwatch_Soldier_Unit
    {
        displayName = "AT Soldier";
        icon = "LSiconAT";
        weapons[] = 
        {
            "Aux501_Weaps_Westar35C",
            "Aux501_Weaps_Westar35SA",
            "Aux501_Weaps_hh12_at",
            "Throw",
            "Put"
        };
        respawnWeapons[] = 
        {
            "Aux501_Weaps_Westar35C",
            "Aux501_Weaps_Westar35SA",
            "Aux501_Weaps_hh12_at",
            "Throw",
            "Put"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",

            "Aux501_Weapons_Mags_hh12_at",
            "Aux501_Weapons_Mags_hh12_he",

            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_Smoke_White"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            
            "Aux501_Weapons_Mags_hh12_at",
            "Aux501_Weapons_Mags_hh12_he",

            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_Smoke_White"
        };
        backpack = "Aux501_Units_Mandalorian_Backpacks_Jumppack_AT";
    };
    class Aux501_Units_Mandalorian_Deathwatch_AA_Unit: Aux501_Units_Mandalorian_Deathwatch_AT_Unit
    {
        displayName = "AA Soldier";
        weapons[] = 
        {
            "Aux501_Weaps_Westar35C",
            "Aux501_Weaps_Westar35SA",
            "Aux501_Weaps_hh12_aa",
            "Throw",
            "Put"
        };
        respawnWeapons[] = 
        {
            "Aux501_Weaps_Westar35C",
            "Aux501_Weaps_Westar35SA",
            "Aux501_Weaps_hh12_aa",
            "Throw",
            "Put"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",

            "Aux501_Weapons_Mags_hh12_aa",

            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_Smoke_White"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35C100",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",

            "Aux501_Weapons_Mags_hh12_aa",

            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_Smoke_White"
        };
        backpack = "Aux501_Units_Mandalorian_Backpacks_Jumppack_AA";
    };
    class Aux501_Units_Mandalorian_Deathwatch_Shotgunner_Unit: Aux501_Units_Mandalorian_Deathwatch_Soldier_Unit
    {
        displayName = "Shotgunner";
        icon = "LSiconAT";
        weapons[] = 
        {
            "Aux501_Weaps_SBB3",
            "Aux501_Weaps_Westar35SA",
            "Throw",
            "Put"
        };
        respawnWeapons[] = 
        {
            "Aux501_Weaps_SBB3",
            "Aux501_Weaps_Westar35SA",
            "Throw",
            "Put"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",

            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",

            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_Smoke_White"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",

            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            "Aux501_Weapons_Mags_Westar35SA30",
            
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_Smoke_White"
        };
    };
    class Aux501_Units_Mandalorian_Deathwatch_Support_Unit: Aux501_Units_Mandalorian_Deathwatch_Soldier_Unit
    {
        displayName = "Support Gunner";
        icon = "LSiconMG";
        weapons[] = 
        {
            "Aux501_Weaps_Westar34L_attach",
            "Throw",
            "Put"
        };
        respawnWeapons[] = 
        {
            "Aux501_Weaps_Westar34L_attach",
            "Throw",
            "Put"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_Westar34L150",
            "Aux501_Weapons_Mags_Westar34L150",
            "Aux501_Weapons_Mags_Westar34L150",
            "Aux501_Weapons_Mags_Westar34L150",
            "Aux501_Weapons_Mags_Westar34L150",
            "Aux501_Weapons_Mags_Westar34L150",
            "Aux501_Weapons_Mags_Westar34L150",
            "Aux501_Weapons_Mags_Westar34L150",

            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_Smoke_White"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_Westar34L150",
            "Aux501_Weapons_Mags_Westar34L150",
            "Aux501_Weapons_Mags_Westar34L150",
            "Aux501_Weapons_Mags_Westar34L150",
            "Aux501_Weapons_Mags_Westar34L150",
            "Aux501_Weapons_Mags_Westar34L150",
            "Aux501_Weapons_Mags_Westar34L150",
            "Aux501_Weapons_Mags_Westar34L150",

            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_Smoke_White"
        };
    };	
};

class CfgGroups
{
    class EAST
    {
        class Aux501_Group_Faction_Mandalorians
        {
            name = "[501st] Mandalorians";
            class Aux501_Group_Editor_Category_Deathwatch
            {
                name = "Deathwatch";
                class Aux501_deathwatch_infantry_squad
                {
                    name = "Infantry Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Mandos";
                    rarityGroup = 1;
                    class Unit0
                    {
                        side = 0;
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1: Unit0
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_AT_Unit";
                        rank = "CORPORAL";
                        position[] = {5,-5,0};
                    };
                    class Unit2: Unit1
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_Soldier_Unit";
                        rank = "PRIVATE";
                        position[] = {-5,-5,0};
                    };
                    class Unit3: Unit2
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_Support_Unit";
                        position[] = {10,-10,0};
                    };
                    class Unit4: Unit2
                    {
                        position[] = {-10,-10,0};
                    };
                    class Unit5: Unit2
                    {
                        position[] = {15,-15,0};
                    };
                    class Unit6: Unit2
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_Shotgunner_Unit";
                        position[] = {-15,-15,0};
                    };
                    class Unit7: Unit3
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_Marksmen_Unit";
                        position[] = {20,-20,0};
                    };
                    class Unit8: Unit2
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_Support_Unit";
                        position[] = {-20,-20,0};
                    };
                };
                class Aux501_deathwatch_weapons_team: Aux501_deathwatch_infantry_squad
                {
                    name = "Weapons Team";
                    class Unit0: Unit0{};
                    class Unit1: Unit1
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_Soldier_Unit";
                    };
                    class Unit2: Unit2{};
                    class Unit3: Unit3
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_AA_Unit";
                    };
                    class Unit4: Unit4{};
                    class Unit5: Unit5
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_AT_Unit_heavy";
                    };
                    class Unit6: Unit6{};

                    class Unit7: Unit7{};
                    
                };
                class Aux501_deathwatch_infantry_team: Aux501_deathwatch_infantry_squad
                {
                    name = "Infantry Team";
                    class Unit0: Unit0{};
                    class Unit1: Unit1{};
                    class Unit2: Unit2{};
                    class Unit3: Unit3{};
                };
                class Aux501_deathwatch_at_team: Aux501_deathwatch_infantry_team
                {
                    name = "AT Team";
                    class Unit0: Unit0{};
                    class Unit1: Unit1
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_AT_Unit";
                    };
                    class Unit2: Unit2
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_AT_Unit";
                    };
                    class Unit3: Unit3
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_AT_Unit";
                    };
                };
                class Aux501_deathwatch_aa_team: Aux501_deathwatch_at_team
                {
                    name = "AA Team";
                    class Unit0: Unit0{};
                    class Unit1: Unit1
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_AT_Unit";
                    };
                    class Unit2: Unit2
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_AA_Unit";
                    };
                    class Unit3: Unit3
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_AA_Unit";
                    };
                };
                class Aux501_deathwatch_infantry_sentry: Aux501_deathwatch_aa_team
                {
                    name = "Infantry Sentry";
                    class Unit0: Unit0
                    {
                        rank = "CORPORAL";
                    };
                    class Unit1: Unit1
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_Soldier_Unit";
                        rank = "PRIVATE";
                    };
                };
                class Aux501_deathwatch_sniper_team: Aux501_deathwatch_infantry_squad
                {
                    name = "Sniper Team";
                    class Unit0: Unit0
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_Marksmen_Unit";
                    };
                    class Unit1: Unit1
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_Marksmen_Unit";
                    };
                };
                class Aux501_deathwatch_platoon_command: Aux501_deathwatch_infantry_team
                {
                    name = "Platoon Command";
                    class Unit0
                    {
                        side = 0;
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_Captain_Unit";
                        rank = "CAPTAIN";
                        position[] = {0,0,0};
                    };
                    class Unit1: Unit0
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {5,-5,0};
                    };
                    class Unit2
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_Soldier_Unit";
                        rank = "PRIVATE";
                        position[] = {-5,-5,0};
                    };
                    class Unit3:Unit2
                    {
                        vehicle = "Aux501_Units_Mandalorian_Deathwatch_Shotgunner_Unit";
                        position[] = {10,-10,0};
                    };
                    class Unit4:Unit3
                    {
                        position[] = {-10,-10,0};
                    };
                };
            };
        };
    };
};