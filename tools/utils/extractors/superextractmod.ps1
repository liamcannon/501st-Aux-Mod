Write-Output "Starting..."

$path = Join-Path $(Get-Location) "addons"

Write-Output $path

$pbos = Get-ChildItem $path -Filter *.pbo
ForEach ($pbo in $pbos)
{
    & "C:\Program Files\PBO Manager v.1.4 beta\PBOConsole.exe" -unpack $pbo.FullName "P:\$($pbo.Name.Split(".")[0])"
}

Write-Output "Finished."