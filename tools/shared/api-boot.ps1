# This file is deisnged to launch the API
# as a separate application, saving the handle,
# and then running the CVI-Runner.ps1 file to spool
# up an instance. This script also ensures
# the Windows Projected File system is enabled.

param(
    [string]$ExePath
)

$path = Resolve-Path -Path $ExePath
$working = Split-Path -Path $path -Parent

Start-Process -FilePath $path -WorkingDirectory $working
Start-Sleep -Seconds 1
