# Contributing to the 501st Aux Mod
Contributions are always welcome, but we do ask you follow some rules when contributing.

Please note these rules are geared towards members of the 501st Aux Mod Team, but all contributions are welcome. If you are not apart of the Aux Mod Team you will be required to create a fork.

## Branches
### Naming
All branches must follow the following naming scheme:

| Branch Type         | Naming Definition                    | Example                              |
|---------------------|--------------------------------------|--------------------------------------|
| **Development**     | `development`                        | `development`                        |
| Feature Development | `dev/<name>/<description>`           | `dev/soyvolon/clone-helms`           |

The primary development branch is `development` and is used for new major feature updates. This branch is the current code for the 501st Aux Mod and is used to publish the mod every week.
- Updates to the development branch will be done on branches following the `dev/<name>/<description>` naming scheme, where `<name>` is the developers name and `<description>` is a very short summary of the feature/bugfix. dev branch names must start with `dev` and is case sensitive. `_` and `.` are not allowed in `dev` names.

### Branch Guidelines
1. Branches should target a single scope - i.e. target a single issue or feature. A branch that aims at solving multiple problems at once will not be allowed to merge with the `development` branch.
2. Branches **must** be based on the correct primary branch. A branch not based on `development` will not be allowed to merge with `development`
    - If you need to `rebase` a branch onto development, see [this page](https://git-scm.com/docs/git-rebase), specifically the `git rebase --onto` section.
4. Branch names are required to be all lowercase.
5. Branches should use a dash `-` to represent spaces.

### Merge Requests
1. Ensure your branch is up to date with the primary branch you intend to merge to. This can be done with a [`rebase`](https://git-scm.com/docs/git-rebase) command.
2. Changes must be properly detailed in the merge request message.
3. Any issues that are closed with the merge request must be noted in the merge request message. Use the format `Closes #<issue number>` (ex. `Closes #12`) to automatically close the issues when the Merge Request is completed.
4. All merge requests must follow the [Testing](#testing) guidelines.

### Testing
The final, unmodified version of the code must be tested before a Merge Request is approved. Tested is defined depending on the nature of the merge:
- For tooling, scripts, other content that doesn't get pushed to the workshop, no testing is required (you can also skip the merge request step if you have permissions).
- For changes that don't affect gameplay at all (textures i.e helmets, purely clientside optional scripts such as awacs) these only need one of any tester to approve additionally to the developer.
- For changes that are small, but affect gameplay, i.e a small balance change, a weapon "fix", a multiplayer server test at minimum, preferably an op but just loading it onto server 9 will do.
- For large config changes, i.e our "projects" tardigrade, phoenix, these require at LEAST two weeks of operational testing on server without any modification. 
- For scripts that have locality concerns, or scripts that could have performance impact, these need to be heavily tested by at least 3-4 operations to ensure no adverse performance effects.

## Commmits

Each commit should only include a single change when possible. Commit messages should be descriptive of what the change being made is. if a commit is the final commit to fix an issue, include the keywords `Closes #<issue number>` (ex. `Closes #12`) in your commit message to automate the closing of issues in the repository.
