#include "config_macros.hpp"

class CfgPatches
{
    class RD501_patch_3den_core
    {
        author=DANKAUTHORS;
		units[]={};
		requiredAddons[]={
            "3DEN"
        };
		requiredVersion=0.1;
        is3DENmod = 1;
    };
};

class Cfg3DEN
{
    class Marker
    {
        class AttributeCategories
        {
            class RD501_3DEN_ZeusMarkers
            {
                displayName = "Zeus Marker Display";
                collapsed = 1;
                class Attributes
                {
                    class RD501_3DEN_ZeusMarkers_ShowOnlyZeus_Attribute
                    {
                        displayName = "Display Only To Zeus";
                        tooltop = "If true, this marker will only be visible to a player with a Zeus module while in a mission.";
                        property = "RD501_3DEN_ZeusMarkers_ShowOnlyZeus_Attribute";
                        control = "Checkbox"; // UI control base class displayed in Edit Attributes window, points to Cfg3DEN >> Attributes

                        // if (not (isNil 'RD501_3DEN_fnc_ZM_markerExpression')) then { [_this, _value] call RD501_3DEN_fnc_ZM_markerExpression; };
                        expression = "[_this, _value] call RD501_3DEN_fnc_ZM_markerExpression;";
                        defaultValue = "false";

                        unique = 0;
                        validate = "none";
                        condition = "1";
                        typeName = "BOOL";
                    };
                };
            };
        };
    };
};