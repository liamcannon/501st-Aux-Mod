params ["_vehicle", "_fuel"];

diag_log text (["[RD501]", "[DEBUG]", "[MC-REFUEL]", "Found target event vehicle:", _vehicle] joinString " ");
if (isNull _vehicle) exitWith {};

diag_log text (["[RD501]", "[DEBUG]", "[MC-REFUEL]", "Setting feul to:", _fuel] joinString " ");
_vehicle setFuel _fuel;