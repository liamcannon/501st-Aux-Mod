# Details
Supports stopping UAVs in mid air and bringing them down to a surface for quick stopping of UAVs returning to the Venator.

## Functions
### Register
`RD501_fnc_init_UAVNET_register`

ex: `[trigger, interactionObject, [obj1, obj2]] call RD501_fnc_init_UAVNET_register`
```sqf
// Registers a UAVNET trigger
params [
	// The trigger, already sized, to save location data for.
	"_thisTrigger", 
	// The objec to add spawn net interactions for.
	"_interactionObject",
	// Any additional objects to save the location of and spawn when the trigger
	// is alive.
	["_additionalSpawns", []]
];
```

This must be placed in the `init` field of an object in a mission file.

### Handle UAV
`RD501_fnc_internal_handleUAV`

ex: `[_oneTrueUAV, _triggerPos] call RD501_fnc_internal_handleUAV`;
```sqf
// Handles UAV landing. Requires a UAV and the trigger position.
params ["_oneTrueUav", "_position"];
```

### Spawn
`RD501_fnc_internal_UAVNET_spawn`

ex: `[] call RD501_fnc_internal_spawn`
```sqf
// Spawns a trigger and its additional objects. Must be called on server.
```

### On Activation
`RD501_fnc_UAVNET_onActivation`

ex: `[thisList, thisTrigger] call RD501_fnc_UAVNET_onActivation`
```sqf
params ["_thisList", "_thisTrigger"];
```

`_thisList` and `_thisTrigger` are the values `thisList` and `thisTrigger` from a trigger activation field. Handles the stopping of a UAV.

### Remove Trigger
`RD501_fnc_UAVNET_removeTrigger`

ex: `[] call RD501_fnc_UAVNET_removeTrigger`
```sqf
// Removes a trigger and its additional objects. Must be called on server.
```

## Events
### Spawn Trigger
`RD501_event_internal_UAVNET_spawnTrigger`

ex: `["RD501_event_internal_UAVNET_spawnTrigger", []] call CBA_fnc_serverEvent;`

### Remove Trigger
`RD501_event_UAVNET_removeTrigger`

ex: `["RD501_event_UAVNET_removeTrigger", []] call CBA_fnc_serverEvent;`

### Handle UAV
`RD501_event_UAVNET_removeTrigger`

ex: `["RD501_event_UAVNET_removeTrigger", [_oneTrueUav, _triggerPos], _oneTrueUav] call CAB_fnc_targetEvent;`