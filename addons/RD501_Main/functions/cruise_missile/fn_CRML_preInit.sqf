settingRes = [
	"RD501_CRML_RandomMissileSpawnAllowed",
    "CHECKBOX",
    ["Random Missile Spawns", "If no launcher is registered, should a random missile be spawned from the sky near the target?"],
    ["RD501", "Cruise Missiles"],
	true,
	true
] call CBA_fnc_addSetting;

settingRes = [
	"RD501_CRML_RandomMissileSpawnReloadSpeed",
    "SLIDER",
    ["Random Missile Reload Speed (s)", "The interval between randomly spawned missiles. Further requests will be queued until the timer expires."],
    ["RD501", "Cruise Missiles"],
	[
		0, // Min
		100, // Max
		10, // Default
		0, // Decimals
		false // % display
	],
	true
] call CBA_fnc_addSetting;

settingRes = [
	"RD501_CRML_RandomMissileWeaponClass",
    "EDITBOX",
    ["Random Missile Weapon Class", "The class name of the weapon to spawn when a missile is fired from a random position."],
    ["RD501", "Cruise Missiles"],
	"RD501_VLS_Launcher_Weapon",
	true
] call CBA_fnc_addSetting;