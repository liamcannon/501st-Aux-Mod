params ["_target", "_player", "_params"];

private _curZeus = missionnamespace getVariable ["RD501_ZL_activeZeusUserObject", objNull];

diag_log text (["[RD501]", "[ZEUS LOGIN]", "DEBUG:", "Attempting logout from Zeus with player", _player, "and current zeus", _curZeus, "with logic gate", _res] joinString " ");

missionNamespace setVariable ["RD501_ZL_activeZeusUser", false, true];
missionNamespace setVariable ["RD501_ZL_activeZeusUserObject", objNull, true];

[_curZeus] remoteExecCall ["RD501_fnc_ZL_revoke", 2];