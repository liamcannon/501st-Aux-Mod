#include "config_macros.hpp"
#include "ui.hpp"

class RscControlsGroup;
class RscFrame;
class RscStructuredText;
class RscTitles {
    class rd501_jumppack_hud {
        idd = 722730;
        duration = 1e+011;
        name = "rd501_jumppack_hud";
        enableDisplay = 1;
        enableSimulation = 1;
        movingEnable = 0;
        onLoad = QUOTE(with uiNamespace do { rd501_jumppack_hud = _this select 0 });
        onUnload = QUOTE(with uiNamespace do { rd501_jumppack_hud = displayNull });
        fadeIn=0;
        fadeOut=0;
        class Controls {
            class rd501_jumppack_details: RscControlsGroup {
                idc = 722731;
                x = 0.860937 * safezoneW + safezoneX;
                y = 0.405 * safezoneH + safezoneY;
                w = 0.144062 * safezoneW;
                h = 0.143 * safezoneH;
                type = CT_CONTROLS_GROUP;
                onLoad = QUOTE(with uiNamespace do { rd501_jumppack_hudDetails = _this select 0 });
                class Controls {
                    class displayNameFrame: RscFrame {
                        idc = 722732;
                        text = "Jumppack Name Goes Here";
                        x = 0 * GUI_GRID_W;
                        y = 0 * GUI_GRID_H;
                        w = 11 * GUI_GRID_W;
                        h = 5 * GUI_GRID_H;
                        colorText[] = {1,1,1,1};
                        colorBackground[] = {0,0,0,0};
                        sizeEx = 1 * GUI_GRID_H;
                        type=CT_STATIC;
                    };
                    class jumpTypeText: RscStructuredText {
                        idc = 722733;
                        text = "Selected Jump Cycle";
                        x = 1 * GUI_GRID_W;
                        y = 1.5 * GUI_GRID_H;
                        w = 9 * GUI_GRID_W;
                        h = 1 * GUI_GRID_H;
                        colorBackground[] = {0,0,0,0};
                        sizeEx = 1 * GUI_GRID_H;
                        type=CT_STRUCTURED_TEXT;
                    };
                    class energyText: RscStructuredText {
                        idc = 722734;
                        text = "Energy: 100/200";
                        x = 1 * GUI_GRID_W;
                        y = 3 * GUI_GRID_H;
                        w = 9 * GUI_GRID_W;
                        h = 1 * GUI_GRID_H;
                        colorText[] = {1,1,1,1};
                        colorBackground[] = {0,0,0,0};
                        sizeEx = 1 * GUI_GRID_H;
                        type=CT_STRUCTURED_TEXT;
                    };
                };
            };
        };
    };
};