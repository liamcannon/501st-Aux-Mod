#include "functions\function_macros.hpp"
#include "\a3\editor_f\Data\Scripts\dikCodes.h"

private _section = "RD501 Jumppack";

[
	_section,
	"Jump",
	[
		"Jump",
		"Press to Jump"
	],
	{
		[player] call FUNC(jump);
	},
	"",
	[DIK_Y, [false, true, false]],
	false
] call cba_fnc_addKeybind;

[
	_section,
	"Cycle jump type left",
	[
		"Cycle jump type left",
		"Cycle jump type left"
	],
	{
		[-1] call FUNC(cycleJumpType);
	},
	"",
	[DIK_G, [false, true, false]],
	false
] call cba_fnc_addKeybind;

[
	_section,
	"Cycle jump type right",
	[
		"Cycle jump type right",
		"Cycle jump type right"
	],
	{
		[1] call FUNC(cycleJumpType);
	},
	"",
	[DIK_J, [false, true, false]],
	false
] call cba_fnc_addKeybind;

private _jumpTypesInCycle = [];
private _settingCategory = ["RD501", "Jumppack Cycle"];

LOGF_1("Generating keybinds, looking through %1 jump types", count GVAR(jumpTypes));
{
	_x params ["_class", "_displayName", "_forward", "_vertical", "_cost", "_relativeToCamera", "_hideFromCycle", "_generateSelectKeybind", "_generateExecuteKeybind"];

	private _settingName = format["%1_%2", QUOTE(GVAR(setting_cycleHide)), _class];
	[
		_settingName,
		"CHECKBOX",
		[
			format[
				"Hide %1 from Cycle",
				_displayName
			],
			format[
				"Will prevent %1 from showing in the cycleable options",
				_displayName
			]
		],
		_settingCategory,
		_hideFromCycle,
		2
	] call CBA_fnc_addSetting;

	GVAR(jumpTypeHideSettings) pushBack _settingName;

	if (!_hideFromCycle) then {
		LOGF_2("%1: %2 can be cycled to", _foreachIndex, _class);
		_jumpTypesInCycle pushBack _x;
	};

	if (_generateSelectKeybind) then {
		private _code = compile format[QUOTE([%1] call FUNC(setJumpType)), _foreachIndex];
		[
			_section,
			format["Select_%1", _class],
			[
				format["Select Jump Type: %1", _displayName],
				format["Immediately selects %1 as your selected jump type instead of having to cycle to it", _displayName]
			],
			_code,
			""
		] call cba_fnc_addKeybind;
		LOGF_2("%1: %2 added select keybind", _foreachIndex, _class);
	};

	if (_generateExecuteKeybind) then {
		private _code = compile format[QUOTE([ARR_2(player, %1)] call FUNC(jump)), _foreachIndex];
		[
			_section,
			format["Execute_%1", _class],
			[
				format["Execute Jump Type: %1", _displayName],
				format["Immediately executes %1 instead of having to cycle to it", _displayName]
			],
			_code,
			""
		] call cba_fnc_addKeybind;
		LOGF_2("%1: %2 Added execute keybind", _foreachIndex, _class);
	};
} forEach GVAR(jumpTypes);

private _cycleCount = count _jumpTypesInCycle;
if (_cycleCount isEqualTo 0) then {
	GVAR(selectedJumpType) = -1;
	GVAR(enableCycle) = false;
	LOG_WARN("There are no jump types that are not hidden from cycle. Cycling is disabled.");
} else {
	LOGF_1("Found %1 jump types that can be cycled to.", _cycleCount);
	GVAR(enableCycle) = true;
	GVAR(selectedJumpType) = GVAR(jumpTypes) findIf {
		_x in _jumpTypesInCycle
	};
	LOGF_1("Selected jump type defaulted to %1", GVAR(selectedJumpType));
};

["ace_interact_menu_newControllableObject", {
	params ["_type"]; // string of the object's className
	private _dispenserIndex = GVAR(dispensers) findIf {
		_x params["_class"];
		_type isKindOf _class
	};
	if (_dispenserIndex isEqualTo -1) exitWith {};

	(GVAR(dispensers) select _dispenserIndex) params ["_dispenserClass", "_jumppackClass"];
	LOGF_2("Read dispenser: %1 with jumppack %2", _dispenserClass, _jumppackClass);
	private _jumppackIndex = GVAR(validBackpacks) findIf {
		_x params ["_class"];
		_jumppackClass isEqualTo _class
	};
	if (_jumppackIndex isEqualTo -1) exitWith {
		LOG_ERRORF_2("Unable to find backpack matching dispenser %1 request %2", _dispenserClass, _jumppackClass);
	};

	private _jumppack = GVAR(validBackpacks) select _jumppackIndex;
	[_type, _jumppack] call FUNC(addDispenserSelfActionsToClass);
}] call CBA_fnc_addEventHandler;

[] call FUNC(addRechargePFH);

["CBA_SettingChanged", FUNC(handleHideSettingsChanged)] call CBA_fnc_addEventHandler;

private _fallHandlerRegistered = isText(configfile >> "ACE_Medical_Injuries" >> "damageTypes" >> "falling" >> "woundHandlers" >> "rd501_jumppack");

if (!_fallHandlerRegistered) then {
	LOG_ERROR("It does not look like the wound handler for jumping was registered correctly. You probably die a bunch as a result of fall damage.");
};
