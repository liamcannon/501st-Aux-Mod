/*
 * Author: M3ales
 * Function to add a recharge per-frame handler (PFH) for the jumppack.
 *
 * Arguments:
 * None
 *
 * Return Value:
 * None
 *
 * Example:
 * [] call fnc_addRechargePFH;
 *
 * Public: No
 */

#include "function_macros.hpp"

private _intervalConfig = configFile >> "JumpTypes" >> "rechargeIntervalSeconds";
private _interval = 3;
if (isNumber(_intervalConfig)) then {
	private _parsed = getNumber(_intervalConfig);
	if (_parsed < 0) exitWith {
		LOG_ERRORF_2("Interval for recharge pfh is parsed as '%1', which is not valid, please ensure the value is over 0, defaulted to %2", _parsed, _interval);
	};
	_interval = _parsed;
};

LOGF_1("Set interval to %1 seconds", _interval);

[
	{
		params["_args", "_id"];
		_args params ["_interval"];

		[] call FUNC(displayHud);
		private _jumppack = [player] call FUNC(getJumppack);
		if (_jumppack isEqualTo false) exitWith {};
		_jumppack params ["_class", "_capacity", "_rechargeRate"];
		private _amountToRecharge = _rechargeRate * _interval;
		[player, _amountToRecharge] call FUNC(rechargeEnergy);
	},
	_interval,
	[_interval]
] call CBA_fnc_addPerFrameHandler;
