/*
 * Author: M3ales
 * Handles changes to the hide settings of jump types.
 *
 * Arguments:
 * 0: Setting to change <STRING>
 * 1: New value for the hideFromCycle property <BOOL>
 *
 * Return Value:
 * None
 *
 * Example:
 * ["my_setting", true] call rd501_jumppack_fnc_handleHideSettingsChanged;
 *
 * Public: No
 */

#include "function_macros.hpp"

params ["_setting", "_value"];

// find the index of the setting in jumpTypeHideSettings
private _hideIndex = GVAR(jumpTypeHideSettings) findIf {
	_x isEqualTo _setting
};

if (_hideIndex isEqualTo -1) exitWith {
	LOGF_1("Setting not found: %1", _setting);
};

// Update the hideFromCycle value for the setting
(GVAR(jumpTypes) select _hideIndex) set [6, _value];
LOGF_2("Setting %1 hideFromCycle to %2", _setting, _value);

private _jumpTypesInCycle = [];

{
	private _x_params = _x;
	_x_params params [
		"_class",
		"_displayName",
		"_forward",
		"_vertical",
		"_cost",
		"_relativeToCamera",
		"_hideFromCycle",
		"_generateSelectKeybind",
		"_generateExecuteKeybind"
	];

	// Add jump type to cycle if not hidden
	if (!_hideFromCycle) then {
		_jumpTypesInCycle pushBack _x;
		LOGF_1("Added jump type to cycle: %1", _displayName);
	};
} forEach GVAR(jumpTypes);

// Determine if cycle should be enabled
GVAR(enableCycle) = count _jumpTypesInCycle != 0;
LOGF_1("Decided that cycle should be enabled: %1", GVAR(enableCycle));
