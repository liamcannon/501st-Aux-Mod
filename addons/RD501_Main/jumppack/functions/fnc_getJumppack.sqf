/*
 * Author: M3ales
 * Retrieves the jumppack for the given unit.
 *
 * Arguments:
 * 0: Unit <OBJECT>
 *
 * Return Value:
 * Jumppack data <ARRAY> or false if not found <BOOL>
 *
 * Example:
 * [player] call rd501_jumppack_fnc_getJumppack;
 *
 * Public: No
 */

#include "function_macros.hpp"

params ["_unit"];

// Get the backpack of the unit
private _backpack = backpack _unit;

// Find the index of the matching backpack in the validBackpacks array
private _matchIndex = GVAR(validBackpacks) findIf {
    _x params ["_class"];
    _class == _backpack
};

// If no matching backpack is found, exit with false
if (_matchIndex == -1) exitWith {
    false
};

// Return the jumppack data from the validBackpacks array
GVAR(validBackpacks) select _matchIndex
