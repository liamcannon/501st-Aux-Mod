/*
 * Author: M3ales
 * Displays the jumppack HUD with energy and jump type information.
 *
 * Arguments:
 * None
 *
 * Return Value:
 * None
 *
 * Example:
 * [] call rd501_jumppack_fnc_displayHud;
 *
 * Public: No
 */

#include "function_macros.hpp"

disableSerialization;

private _unit = player;

private _jumppack = [_unit] call FUNC(getJumppack);

if (_jumppack isEqualTo false) exitWith {
	private _hud = uiNameSpace getVariable ["rd501_jumppack_hud", displayNull];
	if (_hud isNotEqualTo displayNull) then {
		private _details = uiNameSpace getVariable ["rd501_jumppack_hudDetails", displayNull];
		_details ctrlShow false;
	};
};

_jumppack params ["", "_capacity", "", "_displayName"];

private _currentEnergy = (unitBackpack _unit) getVariable [QGVAR(energy), 0];

private _fraction = abs(_currentEnergy / _capacity);

private _energyText = "";
private _jumpTypeText = false;
if (GVAR(enableCycle)) then {
	GVAR(jumpTypes) select GVAR(selectedJumpType) params ["_class", "_displayName"];
	_jumpTypeText = _displayName;
};

_fnc_colour = {
	params ["_fraction"];
	if (_fraction <= 0.25) exitWith {
		"D22D2D"
	};
	if (_fraction <= 0.5) exitWith {
		"F98C1F"
	};
	if (_fraction <= 0.75) exitWith {
		"4DB34D"
	};
	"2986CC"
};

private _energyColourHex = [_fraction] call _fnc_colour;
private _argbColour = [_energyColourHex + "FF"] call BIS_fnc_HEXtoRGB;

_energyText = format["Energy: <t color=""#%3"">%1</t>/%2", round _currentEnergy, round _capacity, _energyColourHex];

private _hud = uiNameSpace getVariable ["rd501_jumppack_hud", displayNull];
if (_hud isEqualTo displayNull) then {
	_hud = "rd501_jumppack_hud" call BIS_fnc_rscLayer;
	_hud cutRsc ["rd501_jumppack_hud", "PLAIN", 0, true];
};

private _details = uiNamespace getVariable ["rd501_jumppack_hudDetails", displayNull];

_details ctrlShow true;
// name
_details controlsGroupCtrl 722732 ctrlSetText _displayName;
_details controlsGroupCtrl 722732 ctrlSetTextColor _argbColour;
_details controlsGroupCtrl 722732 ctrlSetBackgroundColor [0, 0, 0, 1];

// Selected Cycle
if (_jumpTypeText isNotEqualTo false) then {
	_details controlsGroupCtrl 722733 ctrlSetStructuredText parseText _jumpTypeText;
};

// Energy
_details controlsGroupCtrl 722734 ctrlSetStructuredText parseText _energyText;
