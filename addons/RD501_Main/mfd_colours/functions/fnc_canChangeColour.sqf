/*
 * Author: M3ales
 *
 * Arguments:
 * Target
 * Vehicle the action is being performed inside of/on.
 * Player
 * The player performing the action.
 *
 * Return Value:
 * If they are allowed to change the hud colour.
 * It's currently not available for more than the driver in the vehicles we have so its limited to that for now. 
 *
 * Example:
 * [_target, _player] call rd501_mfd_colours_canChangeColour
 * _this call rd501_mfd_colours_canChangeColour
 *
 * Public: No
 */

params["_target", "_player"];
driver _target == _player