#include "functions\function_macros.hpp"

["ace_interact_menu_newControllableObject", {
    params ["_type"]; // string of the object's className
    if (GVAR(validTypes) findIf { _type isKindOf (_x) } == -1) exitWith {};
	[_type] call FUNC(addActionsToClass);
}] call CBA_fnc_addEventHandler;
