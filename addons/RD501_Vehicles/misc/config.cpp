//get generlized macros
#include "../../RD501_main/config_macros.hpp"

class CfgPatches
{
	class RD501_patch_MiscVehicles
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			RD501_GuidenceHelper_W,
            RD501_GuidenceHelper_E
		};
		weapons[]=
		{
			
		};
	};
};

class CfgVehicles
{
    class AllVehicles;
    class RD501_GuidenceHelper_B: AllVehicles
    {
        _generalMacro = "RD501_GuidenceHelper_B";

        author = "RD501";
        displayName = "RD501 Cruise Missile Guidence Helper Object Base";
        destrType="DestructNo";

        icon = "CBA_iconInvisibleTarget";
        mapSize = 0.1;
        model = "\x\cba\addons\ai\InvisibleTarget.p3d";
        scope = 2;
        scopeCurator = 2;
        scopeArsenal = 0;

        simulation="tankX";
        vehicleClass = "Training";

        // Error prevention
        canFloat = 0;
        leftDustEffect = "LDustEffects";
        leftWaterEffect = "LWaterEffects";
        rightDustEffect = "RDustEffects";
        rightWaterEffect = "RWaterEffects";
        class CargoLight {
            ambient = [0.6,0,0.15,1];
            brightness = 0.007;
            color = [0,0,0,0];
        };
        fireDustEffect = "FDustEffects";
        turnCoef = 0;
        class Exhausts {
            class Exhaust1 {
                direction = "exhaust_dir";
                effect = "ExhaustsEffect";
                position = "exhaust";
            };
        };
        selectionBrakeLights = "brzdove svetlo";
        memoryPointMissile = ["spice rakety","usti hlavne"];
        memoryPointMissileDir = ["konec rakety","konec hlavne"];
        memoryPointCargoLight = "cargo light";
        alphaTracks = 0.7;
        textureTrackWheel = 0;
        memoryPointTrack1L = "";
        memoryPointTrack1R = "";
        memoryPointTrack2L = "";
        memoryPointTrack2R = "";
        gearBox = [-1,0,1];
    };

    class RD501_GuidenceHelper_W: RD501_GuidenceHelper_B
    {
        displayName = "BLUFOR Guidence Helper";
        _generalMacro = "RD501_GuidenceHelper_W";
        side=1;

        faction="BLU_F";
        crew="B_UAV_AI";
        typicalCargo[] = {"B_UAV_AI"};
    };

    class RD501_GuidenceHelper_E: RD501_GuidenceHelper_B
    {
        displayName = "OPFOR Guidence Helper";
        _generalMacro = "RD501_GuidenceHelper_E";
        side=2;

        faction="OPF_F";
        crew="O_UAV_AI";
        typicalCargo[] = {"O_UAV_AI"};
    };
};