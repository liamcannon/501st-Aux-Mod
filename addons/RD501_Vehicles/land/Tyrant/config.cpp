//Get this addons macro

//get the macro for the air RD501_patch_vehicles

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

class CfgPatches
{
	class RD501_patch_Tyrant_tank
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			RD501_CIS_Static_AA_LR_ML_Inactive
            RD501_CIS_Static_AA_LR_ML_active
            RD501_CIS_Static_AA_LR
		};
		weapons[]=
		{
			
		};
	};
};

class SensorTemplateActiveRadar;
class SensorTemplateDataLink;
class SensorTemplatePassiveRadar;
class CfgVehicles
{
    class LandVehicle;
    class StaticWeapon: LandVehicle
	{
		class Turrets;
	};
    class StaticMGWeapon : StaticWeapon
    {
        class Turrets: Turrets
        {
            class MainTurret;
        };
    };
    class SAM_System_03_base_F : StaticMGWeapon
    {
        class Turrets: Turrets
		{
			class MainTurret: MainTurret{};
        };
    };

    class B_SAM_System_03_F : SAM_System_03_base_F 
    {
        class Turrets : Turrets {
            class MainTurret: MainTurret{};
        };
    };
    class B_Radar_System_01_F;
    class SAM_System_01_base_F : StaticMGWeapon
    {
        class Turrets: Turrets
		{
			class MainTurret: MainTurret{};
        };
    };
    class B_SAM_System_01_F : SAM_System_01_base_F
    {
        class Turrets : Turrets {
            class MainTurret: MainTurret{};
        };
    };
    class O_Radar_System_02_F;

    //LRAD inactive
    class RD501_CIS_Static_AA_LR_ML_Inactive : B_SAM_System_03_F
    {
        Displayname = "LRAD ML Inactive";
        faction = "RD501_CIS_Faction";
		editorSubcategory = "RD501_Editor_Category_AA";
		vehicleClass = "RD501_Vehicle_Class_AA";
        scope=2;
		scopeCurator=2;
        forceInGarage = 1;
        side = 0;
        radarTarget = 1;
        radarTargetSize = 1;
        irTarget = 1;
        irTargetSize = 1;
        receiveRemoteTargets = 1;
		reportRemoteTargets = 1;
		reportOwnPosition = 1;
        class Turrets : Turrets {
            class MainTurret: MainTurret
            {
                weapons[] = {"RD501_CIS_Static_Missile_AA_LR"};
                magazines[] = {
                    "RD501_CIS_Static_Missile_AA_LR_Mag_4",
                    "RD501_CIS_Static_Missile_AA_LR_Mag_4",
                    "RD501_CIS_Static_Missile_AA_LR_Mag_4",
                    "RD501_CIS_Static_Missile_AA_LR_Mag_4",
                    "RD501_CIS_Static_Missile_AA_LR_Mag_4",
                };
            };
        };
		hiddenSelections[]=
		{
			"camo1",
			"camo2"
		};
		hiddenSelectionsTextures[]=
		{
			"A3\Static_F_Sams\SAM_System_03\Data\sam_system_03_mat_01_CO.paa",
			"A3\Static_F_Sams\SAM_System_03\Data\sam_system_03_mat_02_CO.paa"
		};
        class Components
		{
			class SensorsManagerComponent
			{
				class Components
				{
                    class DataLinkSensorComponent : SensorTemplateDataLink
					{
						class AirTarget
						{
							minRange	= 1;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 1;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
                        groundNoiseDistanceCoef = -1;
                        maxGroundNoiseDistance = 60;
                        minSpeedThreshold = 116;
                        maxSpeedThreshold = 1000;
                        minTrackableSpeed = -1e+10;
                        maxTrackableSpeed = 1e+10;
                        minTrackableATL = 40;
                        maxTrackableATL = 1e+10;
                        typeRecognitionDistance = 16000;
					};
                };
            };
        };

    };

    class RD501_Republic_Static_AA_LR_ML_Inactive : RD501_CIS_Static_AA_LR_ML_Inactive
    {
        faction = "RD501_republic_Faction";
        side = 1;
        class Turrets : Turrets {
            class MainTurret: MainTurret
            {
                weapons[] = {"RD501_CIS_Static_Missile_AA_LR"};
                magazines[] = {
                    "RD501_Republic_Static_Missile_AA_LR_Mag_4",
                    "RD501_Republic_Static_Missile_AA_LR_Mag_4",
                    "RD501_Republic_Static_Missile_AA_LR_Mag_4",
                    "RD501_Republic_Static_Missile_AA_LR_Mag_4",
                    "RD501_Republic_Static_Missile_AA_LR_Mag_4",
                };
            };
        };
		hiddenSelections[]=
		{
			"camo1",
			"camo2"
		};
		hiddenSelectionsTextures[]=
		{
			"A3\Static_F_Sams\SAM_System_03\Data\sam_system_03_mat_01_CO.paa",
			"A3\Static_F_Sams\SAM_System_03\Data\sam_system_03_mat_02_CO.paa"
		};
    };

    //LRAD active
    class RD501_CIS_Static_AA_LR_ML_active : RD501_CIS_Static_AA_LR_ML_Inactive
    {
        Displayname = "LRAD ML Active";
        class Components : Components
		{
			class SensorsManagerComponent
			{
				class Components
				{
					class ActiveRadarSensorComponent: SensorTemplateActiveRadar
                    {
                        class AirTarget {
                            maxRange = 6000;
                            minRange = 6000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        class GroundTarget {
                            maxRange = 6000;
                            minRange = 6000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        aimDown = 0;
                        allowsMarking = 1;
                        angleRangeHorizontal = 360;
                        angleRangeVertical = 180;
                        animDirection = "mainGun";
                        color = [0,1,1,1];
                        componentType = "ActiveRadarSensorComponent";
                        groundNoiseDistanceCoef = -1;
                        maxGroundNoiseDistance = 60;
                        minSpeedThreshold = 116;
                        maxSpeedThreshold = 1000;
                        minTrackableSpeed = -1e+10;
                        maxTrackableSpeed = 1e+10;
                        minTrackableATL = 40;
                        maxTrackableATL = 1e+10;
                        typeRecognitionDistance = 16000;
                    };
				};
			};
		};
    };

    class RD501_Republic_Static_AA_LR_ML_active : RD501_CIS_Static_AA_LR_ML_active
    {
        class Turrets : Turrets {
            class MainTurret: MainTurret
            {
                weapons[] = {"RD501_CIS_Static_Missile_AA_LR"};
                magazines[] = {
                    "RD501_Republic_Static_Missile_AA_LR_Mag_4",
                    "RD501_Republic_Static_Missile_AA_LR_Mag_4",
                    "RD501_Republic_Static_Missile_AA_LR_Mag_4",
                    "RD501_Republic_Static_Missile_AA_LR_Mag_4",
                    "RD501_Republic_Static_Missile_AA_LR_Mag_4",
                };
            };
        };
		hiddenSelections[]=
		{
			"camo1",
			"camo2"
		};
		hiddenSelectionsTextures[]=
		{
			"A3\Static_F_Sams\SAM_System_03\Data\sam_system_03_mat_01_CO.paa",
			"A3\Static_F_Sams\SAM_System_03\Data\sam_system_03_mat_02_CO.paa"
		};
        faction = "RD501_republic_Faction";
        side = 1;
    };
    //LRAD Radar
    class RD501_CIS_Static_AA_LR : B_Radar_System_01_F
    {
        Displayname = "LRAD Radar Station";
        faction = "RD501_CIS_Faction";
		editorSubcategory = "RD501_Editor_Category_AA";
		vehicleClass = "RD501_Vehicle_Class_AA";
        scope=2;
		scopeCurator=2;
        forceInGarage = 1;
        side = 0;
        radarTarget = 1;
        radarTargetSize = 1;
        irTarget = 1;
        irTargetSize = 1;
        receiveRemoteTargets = 1;
		reportRemoteTargets = 1;
		reportOwnPosition = 1;
        hiddenSelections[]=
		{
			"camo1",
			"camo2"
		};
		hiddenSelectionsTextures[]=
		{
			"RD501_Vehicles\land\Tyrant\Data\LRAD_ML_01_CO.paa",
			"RD501_Vehicles\land\Tyrant\Data\LRAD_ML_02_CO.paa"
		};
        class Components
        {
            class SensorsManagerComponent
            {
                class Components
                {
                    class ActiveRadarSensorComponent: SensorTemplateActiveRadar
                    {
                        class AirTarget {
                            maxRange = 15000;
                            minRange = 15000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        class GroundTarget {
                            maxRange = 15000;
                            minRange = 15000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        aimDown = 0;
                        allowsMarking = 1;
                        angleRangeHorizontal = 360;
                        angleRangeVertical = 180;
                        animDirection = "mainGun";
                        color = [0,1,1,1];
                        componentType = "ActiveRadarSensorComponent";
                        groundNoiseDistanceCoef = -1;
                        maxGroundNoiseDistance = 60;
                        minSpeedThreshold = 116;
                        maxSpeedThreshold = 1000;
                        minTrackableSpeed = -1e+10;
                        maxTrackableSpeed = 1e+10;
                        minTrackableATL = 40;
                        maxTrackableATL = 1e+10;
                        typeRecognitionDistance = 16000;
                    };
                    class DataLinkSensorComponent: SensorTemplateDataLink {
                        aimDown = 0;
                        class AirTarget {
                            maxRange = 16000;
                            minRange = 16000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        class GroundTarget {
                            maxRange = 16000;
                            minRange = 16000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        allowsMarking = 1;
                        angleRangeHorizontal = 360;
                        angleRangeVertical = 360;
                        animDirection = "";
                        color = [1,1,1,0];
                        componentType = "DataLinkSensorComponent";
                        groundNoiseDistanceCoef = -1;
                        maxGroundNoiseDistance = 60;
                        minSpeedThreshold = 116;
                        maxSpeedThreshold = 1000;
                        minTrackableSpeed = -1e+10;
                        maxTrackableSpeed = 1e+10;
                        minTrackableATL = 40;
                        maxTrackableATL = 1e+10;
                        typeRecognitionDistance = 16000;
                    };
                };
            };
        };
    };

    class RD501_Republic_Static_AA_LR : RD501_CIS_Static_AA_LR
    {
        faction = "RD501_republic_Faction";
        side = 1;
    };

    //MRC ML
    class RD501_CIS_Static_AA_SR_ML : B_SAM_System_01_F 
    {
        Displayname = "MRC ML Active";
        faction = "RD501_CIS_Faction";
		editorSubcategory = "RD501_Editor_Category_AA";
		vehicleClass = "RD501_Vehicle_Class_AA";
        scope=2;
		scopeCurator=2;
        forceInGarage = 1;
        radarTarget = 1;
        radarTargetSize = 1;
        irTarget = 1;
        irTargetSize = 1;
        side = 0;
        receiveRemoteTargets = 1;
		reportRemoteTargets = 1;
		reportOwnPosition = 1;

        hiddenSelections[]=
		{
			"camo1"
		};
		hiddenSelectionsTextures[]=
		{
			"RD501_Vehicles\land\Tyrant\Data\MRC_ML_01_CO.paa"
		};

        class Turrets : Turrets 
        {
            class MainTurret: MainTurret
            {
                weapons[] = {"RD501_CIS_Static_Missile_AA_SR"};
                magazines[] = {
                    "RD501_CIS_Static_Missile_AA_SR_Mag_21",
                    "RD501_CIS_Static_Missile_AA_SR_Mag_21",
                    "RD501_CIS_Static_Missile_AA_SR_Mag_21",
                    "RD501_CIS_Static_Missile_AA_SR_Mag_21",
                    "RD501_CIS_Static_Missile_AA_SR_Mag_21",
                };
            };
        };

        class Components
		{
			class SensorsManagerComponent
			{
				class Components
				{
                    class DataLinkSensorComponent : SensorTemplateDataLink
					{
						class AirTarget
						{
							minRange	= 1;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 1;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
                        groundNoiseDistanceCoef = -1;
                        maxGroundNoiseDistance = 60;
                        minSpeedThreshold = 116;
                        maxSpeedThreshold = 1000;
                        minTrackableSpeed = -1e+10;
                        maxTrackableSpeed = 1e+10;
                        minTrackableATL = 40;
                        maxTrackableATL = 1e+10;
                        typeRecognitionDistance = 16000;
					};

					class ActiveRadarSensorComponent: SensorTemplateActiveRadar
                    {
                        class AirTarget {
                            maxRange = 4000;
                            minRange = 4000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        class GroundTarget {
                            maxRange = 4000;
                            minRange = 4000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        aimDown = 0;
                        allowsMarking = 1;
                        angleRangeHorizontal = 360;
                        angleRangeVertical = 180;
                        animDirection = "mainGun";
                        color = [0,1,1,1];
                        componentType = "ActiveRadarSensorComponent";
                        groundNoiseDistanceCoef = -1;
                        maxGroundNoiseDistance = 60;
                        minSpeedThreshold = 116;
                        maxSpeedThreshold = 1000;
                        minTrackableSpeed = -1e+10;
                        maxTrackableSpeed = 1e+10;
                        minTrackableATL = 40;
                        maxTrackableATL = 1e+10;
                        typeRecognitionDistance = 16000;
                    };
				};
			};
		};
    };

    class RD501_Republic_Static_AA_SR_ML : RD501_CIS_Static_AA_SR_ML
    {
        faction = "RD501_republic_Faction";
        side = 1;
        class Turrets : Turrets 
        {
            class MainTurret: MainTurret
            {
                weapons[] = {"RD501_CIS_Static_Missile_AA_SR"};
                magazines[] = {
                    "RD501_Republic_Static_Missile_AA_SR_Mag_21",
                    "RD501_Republic_Static_Missile_AA_SR_Mag_21",
                    "RD501_Republic_Static_Missile_AA_SR_Mag_21",
                    "RD501_Republic_Static_Missile_AA_SR_Mag_21",
                    "RD501_Republic_Static_Missile_AA_SR_Mag_21",
                };
            };
        };
    };

    //MRC radar
    class RD501_CIS_Static_AA_SR : O_Radar_System_02_F 
    {
        Displayname = "MRC Radar Station";
        faction = "RD501_CIS_Faction";
		editorSubcategory = "RD501_Editor_Category_AA";
		vehicleClass = "RD501_Vehicle_Class_AA";
        scope=2;
		scopeCurator=2;
        forceInGarage = 1;
        side = 0;
        radarTarget = 1;
        radarTargetSize = 1;
        irTarget = 1;
        irTargetSize = 1;
        receiveRemoteTargets = 1;
		reportRemoteTargets = 1;
		reportOwnPosition = 1;
        class Components
        {
            class SensorsManagerComponent
            {
                class Components
                {
                    class ActiveRadarSensorComponent: SensorTemplateActiveRadar
                    {
                        class AirTarget {
                            maxRange = 10000;
                            minRange = 10000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        class GroundTarget {
                            maxRange = 10000;
                            minRange = 10000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        aimDown = 0;
                        allowsMarking = 1;
                        angleRangeHorizontal = 360;
                        angleRangeVertical = 180;
                        animDirection = "mainGun";
                        color = [0,1,1,1];
                        componentType = "ActiveRadarSensorComponent";
                        groundNoiseDistanceCoef = -1;
                        maxGroundNoiseDistance = 60;
                        minSpeedThreshold = 116;
                        maxSpeedThreshold = 1000;
                        minTrackableSpeed = -1e+10;
                        maxTrackableSpeed = 1e+10;
                        minTrackableATL = 40;
                        maxTrackableATL = 1e+10;
                        typeRecognitionDistance = 16000;
                    };
                    class DataLinkSensorComponent: SensorTemplateDataLink {
                        aimDown = 0;
                        class AirTarget {
                            maxRange = 10000;
                            minRange = 10000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        class GroundTarget {
                            maxRange = 10000;
                            minRange = 10000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        allowsMarking = 1;
                        angleRangeHorizontal = 360;
                        angleRangeVertical = 360;
                        animDirection = "";
                        color = [1,1,1,0];
                        componentType = "DataLinkSensorComponent";
                        groundNoiseDistanceCoef = -1;
                        maxGroundNoiseDistance = 60;
                        minSpeedThreshold = 116;
                        maxSpeedThreshold = 1000;
                        minTrackableSpeed = -1e+10;
                        maxTrackableSpeed = 1e+10;
                        minTrackableATL = 40;
                        maxTrackableATL = 1e+10;
                        typeRecognitionDistance = 16000;
                    };
                };
            };
        };
    };
    
    class RD501_Republic_Static_AA_SR : RD501_CIS_Static_AA_SR
    {
        faction = "RD501_republic_Faction";
        side = 1;
    };

};

class Extended_Init_EventHandlers
{
	class RD501_CIS_Static_AA_LR
	{
        class ERD501_LR_Radar_On
        {
            init = "(_this#0) setVehicleRadar 1;";
        };
	};

    class RD501_Republic_Static_AA_LR
	{
        class ERD501_LR_Radar_On
        {
            init = "(_this#0) setVehicleRadar 1;";
        };
	};

    class RD501_CIS_Static_AA_LR_ML_active
	{
        class ERD501_LR_ML_Radar_On
        {
            init = "(_this#0) setVehicleRadar 1;";
        };
	};

    class RD501_Republic_Static_AA_LR_ML_active
	{
        class ERD501_LR_ML_Radar_On
        {
            init = "(_this#0) setVehicleRadar 1;";
        };
	};

    class RD501_CIS_Static_AA_SR_ML
    {
        class ERD501_SR_ML_Radar_On
        {
            init = "(_this#0) setVehicleRadar 1;";
        };
    };

    class RD501_Republic_Static_AA_SR_ML
    {
        class ERD501_SR_ML_Radar_On
        {
            init = "(_this#0) setVehicleRadar 1;";
        };
    };
    
    class RD501_CIS_Static_AA_SR
    {
        class ERD501_SR_Radar_On
        {
            init = "(_this#0) setVehicleRadar 1;";
        };
    };

    class RD501_Republic_Static_AA_SR
    {
        class ERD501_SR_Radar_On
        {
            init = "(_this#0) setVehicleRadar 1;";
        };
    };

};


