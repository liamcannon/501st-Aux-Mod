//Get this addons macro

//get the macro for the air RD501_patch_vehicles

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon resuppy_box
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

#define macro_new_rs_box_class(name) vehicle_classname##_##name

class CfgPatches
{
    class RD501_patch_resuppy_box
    {
        addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

        requiredAddons[]=
        {
            RD501_patch_vehicles
        };
        requiredVersion=0.1;
        units[]=
        {
            macro_new_vehicle(resuppy_box,small_medical),
            macro_new_vehicle(resuppy_box,small_ammo),
            macro_new_vehicle(resuppy_box,small_uav),
            macro_new_vehicle(resuppy_box,platoon_medical_tent)
        };
        weapons[]=
        {
            
        };
    };
};


#include "../../common/sensor_templates.hpp"
class DefaultEventhandlers;
class CfgVehicles
{
    class 442_box5_1;
    class 442_box10;
    class 442_box4;
    class JLTS_Ammobox_weapons_GAR;

    class macro_new_vehicle(resuppy_box,small_medical): 442_box5_1
    {
        author = "RD501";
        class SimpleObject
        {
            eden = 1;
            animate[] = {};
            hide[] = {};
            verticalOffset = 0.1;
            verticalOffsetWorld = 0;
            init = "''";
        };
        scope = 2;
        scopeCurator = 2;
        displayName = "[501st] Medical Resupply";
        icon = "iconObject_4x5";
        vehicleClass = "Ammo";
        editorCategory = MACRO_QUOTE(macro_editor_cat(suppplies));
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(resupply));
        ACE_maxWeightCarry = 20000;	
        ACE_maxWeightDrag = 20000;
        ace_cargo_size = 1;

            class TransportItems
            {
                #include "../../common/common_items_medical.hpp"				
            };
            class TransportMagazines{};
            class TransportBackpacks{};
        class EventHandlers :DefaultEventhandlers{
            init ="ACE_maxWeightCarry = 20000;ACE_maxWeightDrag = 20000;";
        };
            
    };

    class macro_new_vehicle(resuppy_box,small_ammo): 442_box10
    {
        author = "RD501";
        class SimpleObject
        {
            eden = 1;
            animate[] = {};
            hide[] = {};
            verticalOffset = 0.1;
            verticalOffsetWorld = 0;
            init = "''";
        };
        scope = 2;
        
        scopeCurator = 2;
        displayName = "[501st] Ammo Resupply";
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(resupply));
        icon = "iconObject_4x5";
        vehicleClass = "Ammo";
        editorCategory = MACRO_QUOTE(macro_editor_cat(suppplies));
        ace_cargo_size = 1;
        class TransportItems
        {
            #include "../../common/common_items.hpp"
        };
        class TransportMagazines
        {
            #include "..\..\common\common_ammo_mag.hpp"	
        };	
        class TransportWeapons
        {
            #include "..\..\common\common_weap.hpp"
        };
        class TransportBackpacks{};
    };

    class macro_new_vehicle(resuppy_box,pod_medical): 442_box5_1
    {
        author = "RD501";
        class SimpleObject
        {
            eden = 1;
            animate[] = {};
            hide[] = {};
            verticalOffset = 0.1;
            verticalOffsetWorld = 0;
            init = "''";
        };
        scope = 2;
        scopeCurator = 2;
        displayName = "[501st] Medical Resupply (Pod)";
        icon = "iconObject_4x5";
        vehicleClass = "Ammo";
        editorCategory = MACRO_QUOTE(macro_editor_cat(suppplies));
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(resupply));
        ACE_maxWeightCarry = 20000;	
        ACE_maxWeightDrag = 20000;
        ace_cargo_size = 1;

            class TransportItems
            {
                #include "../../common/common_items_medical_pod.hpp"				
            };
            class TransportMagazines{};
            class TransportBackpacks{};
        class EventHandlers :DefaultEventhandlers{
            init ="ACE_maxWeightCarry = 20000;ACE_maxWeightDrag = 20000;";
        };
            
    };

    class macro_new_vehicle(resuppy_box,pod_ammo): 442_box10
    {
        author = "RD501";
        class SimpleObject
        {
            eden = 1;
            animate[] = {};
            hide[] = {};
            verticalOffset = 0.1;
            verticalOffsetWorld = 0;
            init = "''";
        };
        scope = 2;
        
        scopeCurator = 2;
        displayName = "[501st] Ammo Resupply (Pod)";
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(resupply));
        icon = "iconObject_4x5";
        vehicleClass = "Ammo";
        editorCategory = MACRO_QUOTE(macro_editor_cat(suppplies));
        ace_cargo_size = 1;
        class TransportItems
        {
            #include "../../common/common_items_pod.hpp"
        };
        class TransportMagazines
        {
            #include "..\..\common\common_ammo_mag_pod.hpp"	
        };	
        class TransportWeapons{};
        class TransportBackpacks{};
    };

    class Aux501_turret_resupply_box_ammo: JLTS_Ammobox_weapons_GAR
    {
        author = "501st Aux Team";
        class SimpleObject
        {
            eden = 1;
            animate[] = {};
            hide[] = {};
            verticalOffset = 0.1;
            verticalOffsetWorld = 0;
            init = "''";
        };
        scope = 2;
        scopeCurator = 2;
        displayName = "[501st] Turret Resupply";
        editorSubcategory = "RD501_Editor_Category_resupply";
        icon = "iconObject_4x5";
        vehicleClass = "Ammo";
        editorCategory = "RD501_Editor_Category_suppplies";
        ace_cargo_size = 1;
        ACE_maxWeightCarry = 20000;	
        ACE_maxWeightDrag = 20000;
        class TransportItems{};
        class TransportMagazines
        {
            //eweb ammo
            class _xx_Aux501_Weapons_Mags_EWHB12_MG_csw
            {
                magazine = "Aux501_Weapons_Mags_EWHB12_MG_csw";
                count = 10;
            };
            //boomer ammo
            class _xx_Aux501_Weapons_Mags_EWHB12_GL_csw
            {
                magazine = "Aux501_Weapons_Mags_EWHB12_GL_csw";
                count = 10;
            };
            //striker ammo
            class _xx_Aux501_Weapons_Mags_AAP4_csw
            {
                magazine = "Aux501_Weapons_Mags_AAP4_csw";
                count = 10;
            };
            //driver ammo
            class _xx_Aux501_Weapons_Mags_mar1_csw
            {
                magazine = "Aux501_Weapons_Mags_mar1_csw";
                count = 10;
            };
            //Mortar ammo
            class _xx_Aux501_Weapons_Mags_81mm_mortar_HE_csw
            {
                magazine = "Aux501_Weapons_Mags_81mm_mortar_HE_csw";
                count = 10;
            };
            class _xx_Aux501_Weapons_Mags_81mm_mortar_smoke_csw
            {
                magazine = "Aux501_Weapons_Mags_81mm_mortar_smoke_csw";
                count = 10;
            };
            class _xx_Aux501_Weapons_Mags_81mm_mortar_illum_csw
            {
                magazine = "Aux501_Weapons_Mags_81mm_mortar_illum_csw";
                count = 10;
            };
            class _xx_Aux501_Weapons_Mags_81mm_mortar_guided_HE_csw
            {
                magazine = "Aux501_Weapons_Mags_81mm_mortar_guided_HE_csw";
                count = 10;
            };
            class _xx_Aux501_Weapons_Mags_81mm_mortar_laserguided_HE_csw
            {
                magazine = "Aux501_Weapons_Mags_81mm_mortar_laserguided_HE_csw";
                count = 10;
            };
        };
        class TransportWeapons
        {
            class _xx_Aux501_Weaps_EWHB12_carry
            {
                weapon = "Aux501_Weaps_EWHB12_carry";
                count = 1;
            };
            class _xx_Aux501_Weaps_AAP4_carry
            {
                weapon = "Aux501_Weaps_AAP4_carry";
                count = 1;
            };
            class _xx_Aux501_Weaps_MAR1_carry
            {
                weapon = "Aux501_Weaps_MAR1_carry";
                count = 1;
            };
            class _xx_Aux501_Weaps_81mm_plasma_Mortar_Carry
            {
                weapon = "Aux501_Weaps_81mm_plasma_Mortar_Carry";
                count = 1;
            };
        };
        class TransportBackpacks{};
        class EventHandlers :DefaultEventhandlers
        {
            init ="ACE_maxWeightCarry = 20000; ACE_maxWeightDrag = 20000;";
        };
    };


    class macro_new_vehicle(resuppy_box,small_uav): 442_box4
    {
        author = "RD501";
        class SimpleObject
        {
            eden = 1;
            animate[] = {};
            hide[] = {};
            verticalOffset = 0.1;
            verticalOffsetWorld = 0;
            init = "''";
        };
        scope = 2;
        
        scopeCurator = 2;
        displayName = "[501st] UAV Resupply";
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(resupply));
        vehicleClass = "Ammo";
        editorCategory = MACRO_QUOTE(macro_editor_cat(suppplies));
        maxload=1000000000;
        ace_cargo_size = 2;
        ace_cargo_canLoad = 1;  
        class TransportBackpacks
            {
                class _transport_r2
                    {
                        backpack=MACRO_QUOTE(macro_new_vehicle(drone,Clone_Recon_bag));
                        count=1;
                    };
                class _transport_eddie
                    {
                        backpack="B_UGV_02_Demining_backpack_F";
                        count=1;
                    };
                class TransportMagazines{};
            };
        class TransportItems{};
        class TransportMagazines{};
        class TransportWeapons{};
        class EventHandlers :DefaultEventhandlers{
            init ="ACE_maxWeightCarry = 20000;ACE_maxWeightDrag = 20000;";
        };
    };
};