//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

class CfgPatches
{
	class RD501_patch_VLS
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
            RD501_VLS_System,
            RD501_VLS_Launcher_Helper_Weapon
		};
		weapons[]=
		{
			
		};
	};
};

class CfgVehicles
{
    // Imports
    class StaticWeapon;
    class StaticMGWeapon: StaticWeapon
    {
        class Turrets;
    };
    class B_Ship_MRLS_01_base_F: StaticMGWeapon
    {
        class Turrets: Turrets
        {
            class MainTurret;
        };
    };

    // VLS System
    class RD501_VLS_System: B_Ship_MRLS_01_base_F
    {
        scope = 2;
        scopeCurator = 2;
        side = 3;

        author = MACRO_AUTHOR;
        displayName = "Republic VLS";
        faction = MACRO_QUOTE(macro_faction(republic));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(arty));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(arty));

        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                weapons[] = {"RD501_VLS_Launcher_Weapon"};
                magazines[] = {"RD501_Cruise_Missile_Magazine"};
            };
        };

        crew = "C_UAV_AI_F";
		typicalCargo[] = {"C_UAV_AI_F"};

        // From B_Ship_MRLS_01_F
        class SimpleObject
		{
			eden = 1;
			animate[] = {{"mainturret",0},{"maingun",0.26},{"hide_missilecover_01",1},{"hide_missilecover_02",1},{"hide_missilecover_03",1},{"hide_missilecover_04",1},{"hide_missilecover_05",1},{"hide_missilecover_06",1},{"hide_missilecover_07",1},{"hide_missilecover_08",1},{"damageturret",0},{"damage_hide_missilecover_01",0},{"damage_hide_missilecover_02",0},{"damage_hide_missilecover_03",0},{"damage_hide_missilecover_04",0},{"damage_hide_missilecover_05",0},{"damage_hide_missilecover_06",0},{"damage_hide_missilecover_07",0},{"damage_hide_missilecover_08",0},{"missile_move_1",1},{"missile_move_2",1},{"missile_move_3",1},{"missile_move_4",1},{"missile_move_5",1},{"missile_move_6",1},{"missile_move_7",1}};
			hide[] = {"zasleh","light_back","brzdove svetlo","clan","podsvit pristroju","poskozeni"};
			verticalOffset = 1.972;
			verticalOffsetWorld = 0.001;
			init = "''";
		};

        class AnimationSources
		{
			class Missiles_revolving
			{
				source = "ammo";
				weapon = "RD501_VLS_Launcher_Weapon";
				animPeriod = 0.001;
			};
		};
    };

    class RD501_VLS_MASH_System: B_Ship_MRLS_01_base_F
    {
        scope = 2;
        scopeCurator = 2;
        side = 3;

        author = MACRO_AUTHOR;
        displayName = "Republic MASH VLS";
        faction = MACRO_QUOTE(macro_faction(republic));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(arty));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(arty));

        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                weapons[] = {"RD501_VLS_Launcher_Weapon"};
                magazines[] = {"RD501_Mash_Missile_Magazine"};
            };
        };

        crew = "C_UAV_AI_F";
		typicalCargo[] = {"C_UAV_AI_F"};

        // From B_Ship_MRLS_01_F
        class SimpleObject
		{
			eden = 1;
			animate[] = {{"mainturret",0},{"maingun",0.26},{"hide_missilecover_01",1},{"hide_missilecover_02",1},{"hide_missilecover_03",1},{"hide_missilecover_04",1},{"hide_missilecover_05",1},{"hide_missilecover_06",1},{"hide_missilecover_07",1},{"hide_missilecover_08",1},{"damageturret",0},{"damage_hide_missilecover_01",0},{"damage_hide_missilecover_02",0},{"damage_hide_missilecover_03",0},{"damage_hide_missilecover_04",0},{"damage_hide_missilecover_05",0},{"damage_hide_missilecover_06",0},{"damage_hide_missilecover_07",0},{"damage_hide_missilecover_08",0},{"missile_move_1",1},{"missile_move_2",1},{"missile_move_3",1},{"missile_move_4",1},{"missile_move_5",1},{"missile_move_6",1},{"missile_move_7",1}};
			hide[] = {"zasleh","light_back","brzdove svetlo","clan","podsvit pristroju","poskozeni"};
			verticalOffset = 1.972;
			verticalOffsetWorld = 0.001;
			init = "''";
		};

        class AnimationSources
		{
			class Missiles_revolving
			{
				source = "ammo";
				weapon = "RD501_VLS_Launcher_Weapon";
				animPeriod = 0.001;
			};
		};
    };

    class RD501_VLS_Launcher_Helper_Weapon: B_Ship_MRLS_01_base_F
    {
        author = MACRO_AUTHOR;
        displayName = "VLS Launch Helper";
        // mapSize = 0.1;
        // model = "\x\cba\addons\ai\InvisibleTarget.p3d";

        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                weapons[] = {};
                magazines[] = {};
            };
        };

        crew = "C_UAV_AI_F";
		typicalCargo[] = {"C_UAV_AI_F"};

        scope = 2;
        scopeCurator = 1;
        scopeArsenal = 1;
    };
};

class Extended_InitPost_EventHandlers {
    class RD501_VLS_System {
        class VLS_InitPost_EventHandler {
            init = "_this call RD501_fnc_CRML_vlsInit;";
        };
    };
    class RD501_VLS_MASH_System {
        class VLS_MASH_InitPost_EventHandler {
            init = "_this call RD501_fnc_CRML_vlsInit;";
        };
    };
    class RD501_VLS_Launcher_Helper_Weapon {
        class VLSHelper_InitPost_EventHandler {
            init = "_this call RD501_fnc_CRML_vlsInit;";
        };
    };
};