class Object0	{side = 8; vehicle = "k_fence_ray_stand_gate"; rank = ""; position[] = {0.0516357,-0.00739479,0}; dir = -179.036;};
class Object1	{side = 8; vehicle = "land_3as_cis_wall_corner_v2"; rank = ""; position[] = {14.9412,2.08301,0}; dir = 0.964188;};
class Object2	{side = 8; vehicle = "land_3as_cis_wall_bunker_v2"; rank = ""; position[] = {19.7094,16.2929,0}; dir = -89.0358;};
class Object3	{side = 8; vehicle = "land_3as_cis_wall_corner_v2"; rank = ""; position[] = {19.8411,26.0796,0}; dir = -89.0358;};
class Object4	{side = 8; vehicle = "land_3as_cis_wall_tower_v2"; rank = ""; position[] = {-8.66479,31.3508,4.76837e-007}; dir = 180.964;};
class Object5	{side = 8; vehicle = "k_fence_ray_stand_gate"; rank = ""; position[] = {0.510742,31.1993,0}; dir = 0.964188;};
class Object6	{side = 8; vehicle = "land_3as_cis_wall_tower_v2"; rank = ""; position[] = {9.81946,31.0622,4.76837e-007}; dir = 180.964;};
class Object7	{side = 8; vehicle = "land_3as_cis_wall_corner_v2"; rank = ""; position[] = {-14.2765,31.2997,0}; dir = 180.964;};
class Object8	{side = 8; vehicle = "land_3as_cis_wall_bunker_v2"; rank = ""; position[] = {-19.0447,17.0897,0}; dir = 90.9642;};
class Object9	{side = 8; vehicle = "land_3as_cis_wall_corner_v2"; rank = ""; position[] = {-19.1764,7.30305,0}; dir = 90.9642;};
class Object10	{side = 8; vehicle = "land_3as_cis_bunker_v2"; rank = ""; position[] = {-8.60974,1.55137,3.07204}; dir = 0.964188;};
class Object11	{side = 8; vehicle = "land_3as_cis_bunker_v2"; rank = ""; position[] = {8.6781,1.25895,3.07204}; dir = 0.964188;};
class Object12	{side = 8; vehicle = "3AS_Barricade_cis_prop"; rank = ""; position[] = {-16.6952,24.2598,0}; dir = 0.964233;};
class Object13	{side = 8; vehicle = "3AS_Barricade_cis_prop"; rank = ""; position[] = {-16.9425,9.82771,0}; dir = 0.964233;};
class Object14	{side = 8; vehicle = "3AS_Barricade_cis_prop"; rank = ""; position[] = {17.5854,23.6335,0}; dir = -179.036;};
class Object15	{side = 8; vehicle = "3AS_Barricade_cis_prop"; rank = ""; position[] = {17.3381,9.20143,0}; dir = -179.036;};
class Object16	{side = 8; vehicle = "3AS_H_barrier_small_5"; rank = ""; position[] = {6.21716,9.64111,0}; dir = -89.0358;};
class Object17	{side = 8; vehicle = "3AS_H_barrier_1"; rank = ""; position[] = {6.61743,5.36949,0}; dir = -89.0358;};
class Object18	{side = 8; vehicle = "3AS_H_barrier_1"; rank = ""; position[] = {6.76245,13.9002,0}; dir = -89.0358;};
class Object19	{side = 8; vehicle = "3AS_H_barrier_small_5"; rank = ""; position[] = {-5.86365,9.84981,0}; dir = 90.9642;};
class Object20	{side = 8; vehicle = "3AS_H_barrier_1"; rank = ""; position[] = {-6.26416,14.1214,0}; dir = 90.9642;};
class Object21	{side = 8; vehicle = "3AS_H_barrier_1"; rank = ""; position[] = {-6.40918,5.59068,0}; dir = 90.9642;};